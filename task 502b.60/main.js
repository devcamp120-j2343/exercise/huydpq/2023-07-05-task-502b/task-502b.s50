import { Vehice } from "./vehice.js";
import Car from "./car.js";
import { Motorbike } from "./motorbike.js";

let vehice = new Vehice("BMW",2022);
let car = new Car("350i", "aaaa")
let motorbike = new Motorbike("Ducati", "aaaa")

console.log(vehice instanceof Vehice);
console.log(car instanceof Vehice);
console.log(car instanceof Car)
console.log(motorbike instanceof Motorbike);
console.log(motorbike instanceof Vehice)